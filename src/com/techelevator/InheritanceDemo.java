package com.techelevator;

public class InheritanceDemo {
	public static void main(String[] args) {
		
		// Since all classes extend java.lang.Object, we can assign an instance of any class to a variable of type Object
		
		String hello = "Hello!";
		Object myObject = hello;
		System.out.println("myObject is : "+myObject);
		
		// myObject.toUpperCase();   // this is a compile error because even though myObject is referencing an instance of String, the declared variable type is java.lang.Object
		
		Integer two = new Integer(2);
		myObject = two;
		System.out.println("myObject is : "+myObject);
		
		String twoString = "2";
		boolean b = twoString.equals(two);		// comparing instances of two different classes is allowed because the equals method takes an Object as an argument
		System.out.println("twoString.equals(two) : "+b);
		
		// The default implementation of the toString method is to print the fully qualified class name followed by a memory address
		Rectangle myRectangle = new Rectangle(4, 5);
		String myRectangleString = myRectangle.toString();
		System.out.println("myRectangleString : "+myRectangleString);
		
		// The default implementation of the equals method is to compare object references (i.e. memory addresses)
		// In other words, it's the same as saying myRectangle == myOtherRectangle
		Rectangle myOtherRectangle = new Rectangle(4, 5);
		boolean rectanglesAreEqual = myRectangle.equals(myOtherRectangle);
		System.out.println("rectanglesAreEqual : "+rectanglesAreEqual);
		System.out.println("myRectanlge.equals(myRectangle) : "+myRectangle.equals(myRectangle));
		
		String goodbyeString = "Goodbye!";
		Object goodbyeObject = goodbyeString;  // Again, this assignment is allowed because a String "is-a" Object.  I can assign a value of type String to a variable of type Object
		// String anotherString = goodbyeObject; // this is a compile error because an Object is not necessarily a String (it could be an Integer, or an ArrayList) so I cannot blindly assign a value of type Object to a variable of type String
		String anotherString = (String)goodbyeObject;  // we can use "casting" to force goodbyeObject to be treated as a String
		System.out.println("anotherString is : "+anotherString);
		
		Object fiveObject = new Integer(5);
		// String fiveString = (String)fiveObject;  // this will cause a java.lang.ClassCastException because it's invalid to cast an Integer to a String
		
		Integer nineInteger = new Integer(9);
		// String nineString = (String)nineInteger;  // this is a compiler error because the compiler can tell that there's no possible way an Integer can store a value compatible with type String
		
		System.out.println("\n###### Overriding java.lang.Object methods in a subclass #######");
		RectangleWithOverriddenMethods betterRectangle = new RectangleWithOverriddenMethods(3, 5);
		System.out.println("betterRectangle is : "+betterRectangle);
		
		RectangleWithOverriddenMethods anotherBetterRectangle = new RectangleWithOverriddenMethods(3, 5);
		boolean betterRectanglesAreEqual = betterRectangle.equals(anotherBetterRectangle);
		System.out.println("betterRectangle.equals(anotherBetterRectangle) : "+betterRectanglesAreEqual);
		betterRectanglesAreEqual = betterRectangle.equals(null);
		System.out.println("betterRectangle.equals(null) : "+betterRectanglesAreEqual);
		betterRectanglesAreEqual = betterRectangle.equals(betterRectangle);
		System.out.println("betterRectangle.equals(betterRectangle) : "+betterRectanglesAreEqual);
		
		betterRectangle.equals("I am not a Rectangle!");
	}
}
