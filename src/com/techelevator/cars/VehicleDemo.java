package com.techelevator.cars;

import com.techelevator.cars.ford.Focus;
import com.techelevator.cars.ford.FocusHatchback;
import com.techelevator.cars.subaru.Outback;

public class VehicleDemo {

	public static void main(String[] args) {
		
		System.out.println("#### StationWagon #####");
		Vehicle myStationWagon = new StationWagon();
		System.out.println(myStationWagon+" has "+myStationWagon.getNumberOfDoors()+" doors");
		System.out.println(myStationWagon+" has "+myStationWagon.getNumberOfWheels()+" wheels");
		if(myStationWagon.isAWD()) {
			System.out.println(myStationWagon+" is all-wheel-drive");
		} else {
			System.out.println(myStationWagon+" is not all-wheel-drive");
		}
		
		System.out.println("\n#### Subaru Outback #####");
		Vehicle myOutback = new Outback();
		System.out.println(myOutback+" has "+myOutback.getNumberOfDoors()+" doors");
		System.out.println(myOutback+" has "+myOutback.getNumberOfWheels()+" wheels");
		if(myOutback.isAWD()) {
			System.out.println(myOutback+" is all-wheel-drive");
		} else {
			System.out.println(myOutback+" is not all-wheel-drive");
		}
		
		System.out.println("\n#### Ford Focus #####");
		Vehicle myFocus = new Focus();
		System.out.println(myFocus+" has "+myFocus.getNumberOfDoors()+" doors");
		System.out.println(myFocus+" has "+myFocus.getNumberOfWheels()+" wheels");
		if(myFocus.isAWD()) {
			System.out.println(myFocus+" is all-wheel-drive");
		} else {
			System.out.println(myFocus+" is not all-wheel-drive");
		}
		
		System.out.println("\n#### Ford Focus Hatchback #####");
		Vehicle myFocusHatchback = new FocusHatchback();
		System.out.println(myFocusHatchback+" has "+myFocusHatchback.getNumberOfDoors()+" doors");
		System.out.println(myFocusHatchback+" has "+myFocusHatchback.getNumberOfWheels()+" wheels");
		if(myFocusHatchback.isAWD()) {
			System.out.println(myFocusHatchback+" is all-wheel-drive");
		} else {
			System.out.println(myFocusHatchback+" is not all-wheel-drive");
		}
	}

}
