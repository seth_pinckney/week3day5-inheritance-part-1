package com.techelevator.cars.ford;

public class FocusHatchback extends Focus {
	
	public int getNumberOfDoors( ) {
		System.out.println("*** FocusHatchback.getNumberOfDoors");
		return super.getNumberOfDoors() + 1;
	}

	public String toString() {
		return super.toString() + " Hatchback";
	}
}
