package com.techelevator.cars.ford;

import com.techelevator.cars.Vehicle;

public class Focus implements Vehicle {

	public int getNumberOfWheels() {
		System.out.println("*** Focus.getNumberOfWheels");
		return 4;
	}

	public int getNumberOfDoors() {
		System.out.println("*** Focus.getNumberOfDoors");
		return 4;
	}

	public boolean isAWD() {
		System.out.println("*** Focus.isAWD");
		return false;
	}

	public String toString() {
		return "Ford Focus";
	}
}
