package com.techelevator.cars;

public class StationWagon implements Vehicle {

	public final int getNumberOfWheels() { // The "final" keyword here indicates that this method cannot be overridden by sub-classes
		System.out.println("*** StationWagon.getNumberOfWheels");
		return 4;
	}

	public int getNumberOfDoors() {
		System.out.println("*** StationWagon.getNumberOfDoors");
		return 5;
	}

	public boolean isAWD() {
		System.out.println("*** StationWagon.isAWD");
		return false;
	}

	public String toString() {
		return "Station Wagon";
	}
}
