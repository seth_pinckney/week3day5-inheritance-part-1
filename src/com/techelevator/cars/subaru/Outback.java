package com.techelevator.cars.subaru;

import com.techelevator.cars.StationWagon;

public final class Outback extends StationWagon {

	/*public int getNumberOfWheels() { // this is a compiler error because StationWagon.getNumberOfWheels is declared as "final" and therefore cannot be overridden
		return 6;
	}*/
	
	public boolean isAWD() {
		System.out.println("*** Outback.isAWD");
		return true;
	}
	
	public String toString() {
		return "Subaru Outback";
	}
}
