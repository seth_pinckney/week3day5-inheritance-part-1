package com.techelevator.cars;

public interface Vehicle {
	int getNumberOfWheels();
	int getNumberOfDoors();
	boolean isAWD();
}
