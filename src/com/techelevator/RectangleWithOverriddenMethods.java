package com.techelevator;

public class RectangleWithOverriddenMethods {
	private int length;
	private int width;
	
	public RectangleWithOverriddenMethods(int length, int width) {
		this.length = length;
		this.width = width;
	}
	
	public int getArea() {
		return length * width;
	}
	
	// Here we are overriding the toString method from java.lang.Object
	public String toString() {
		return "Rectangle( length: "+length+", width: "+width+")";
	}

	// Here we are overriding the equals method from java.lang.Object
	public boolean equals(Object obj) {
		boolean isEqual;
		if(obj == null) {    				// equals method says that no object can be equal to null
			isEqual = false;
		} else if(this == obj) {			// equals method says that equality is "reflexive" (i.e. for a non-null object x, x.equals(x) must be true
			isEqual = true;
		} else {
			if( obj instanceof RectangleWithOverriddenMethods) {										// The instanceof operator can be used to check the type of an object
				RectangleWithOverriddenMethods otherRectangle = (RectangleWithOverriddenMethods)obj;
				isEqual = this.length == otherRectangle.length && this.width == otherRectangle.width;
			} else {
				isEqual = false;
			}
		}
		return isEqual;
	}
}
